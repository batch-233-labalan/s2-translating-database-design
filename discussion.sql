
SHOW DATABASES;
CREATE DATABASE music_db;
USE music_db;

SHOW TABLES;


CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCGAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);


CREATE TABLE artist (
   id INT NOT NULL AUTO_INCREMENT,
   username VARCHAR(50) NOT NULL,
   PRIMARY KEY (id)
);


CREATE TABLE albums (
   id INT NOT NULL AUTO_INCREMENT,
   album_title VARCHAR(50) NOT NULL,
   date_released DATE NOT NULL,
   artist_id INT NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT fk_albums_artist_id
   		FOREIGN KEY (artist_id) REFERENCES artist(id)
   		ON UPDATE CASCADE
   		ON DELETE RESTRICT
);


CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
    	FOREIGN KEY(album_id) REFERENCES albums(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);


CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY(user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);



CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlists_id INT NOT NULL,
	songs_id INT NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_songs_playlists_id
		FOREIGN KEY (playlists_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_songs_id
		FOREIGN KEY (songs_id) REFERENCES songs(id)
		ON UPDATE CASCADE
    	ON DELETE RESTRICT
);


-- REVIEW:

CREATE DATABASE tasksdb;

USE tasksdb;

CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
username VARCHAR(50) NOT NULL,
password VARCHAR(25) NOT NULL,
PRIMARY KEY (id)
);


CREATE TABLE tasks (
id INT NOT NULL AUTO_INCREMENT,
status VARCHAR(25) NOT NULL,
name VARCHAR(50) NOT NULL,
users_id INT NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_users
	FOREIGN KEY (users_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);










